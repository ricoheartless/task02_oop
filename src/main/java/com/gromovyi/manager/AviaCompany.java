package com.gromovyi.manager;

import com.gromovyi.model.Aeroplane;
import com.gromovyi.utility.Range;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class AviaCompany {

  private List<Aeroplane> aeroplanes;

  public AviaCompany(final List<Aeroplane> list) {
    this.aeroplanes = list;
  }

  public final void sortByDistance() {
    this.aeroplanes.sort((o1, o2) -> o1.getTravelDistance() - o2.getTravelDistance());
  }

  public final List<Aeroplane> findByFuelConsume(final Range range) {
    List<Aeroplane> resultList = new LinkedList<>();
    aeroplanes.stream()
        .filter(plane -> range.contains(plane.getFuelTank()))
        .forEachOrdered(plane -> {
          resultList.add(plane);
        });
    return resultList;
  }

  public final void show() {
    int i = 0;
    for (Aeroplane a : aeroplanes) {
      System.out.println(
          "Aeroplane #" + i
              + " Capacity: " + a.getCapacity()
              + ", Fuel: " + a.getFuelTank()
              + ", Travel distance: " + a.getTravelDistance());
      ++i;
    }
  }

  public final int getSumOfCapacity() {
    int sum = 0;
    for (Aeroplane a : aeroplanes) {
        sum += a.getCapacity();
    }
    return sum;
  }

  public final int getSumOfCargoLoad() {
    int sum = 0;
    for (Aeroplane a : aeroplanes) {
      sum += a.getLoadCapacity();
    }
    return sum;
  }
}
