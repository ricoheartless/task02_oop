package com.gromovyi.utility;

import com.gromovyi.model.Aeroplane;
import com.gromovyi.model.CargoPlane;
import com.gromovyi.model.PassengerPlane;
import java.util.LinkedList;
import java.util.List;

public final class Creator {

  private Creator() {
  }

  public static final List<Aeroplane> createAeroplanes() {
    List<Aeroplane> aeroplanes = new LinkedList<>();
    Aeroplane a1 = new PassengerPlane(10, 10, 20, 500);
    Aeroplane a2 = new PassengerPlane(50, 10, 60, 200);
    Aeroplane a3 = new CargoPlane(20, 10, 15, 700);
    Aeroplane a4 = new PassengerPlane(60, 10, 100, 450);
    aeroplanes.add(a1);
    aeroplanes.add(a2);
    aeroplanes.add(a3);
    aeroplanes.add(a4);
    return aeroplanes;
  }

}
