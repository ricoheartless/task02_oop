package com.gromovyi.model;

public abstract class Aeroplane {

  private int capacity;
  private int loadCapacity;
  private int fuelTank;
  private int travelDistance;

  Aeroplane(final int capacity, final int loadCapacity, final int fuelTank,
      final int travelDistance) {
    this.capacity = capacity;
    this.loadCapacity = loadCapacity;
    this.fuelTank = fuelTank;
    this.travelDistance = travelDistance;
  }

  public final int getFuelTank() {
    return fuelTank;
  }

  public final void setFuelTank(final int fuelTank) {
    this.fuelTank = fuelTank;
  }

  public final int getCapacity() {
    return capacity;
  }

  public final int getLoadCapacity() {
    return loadCapacity;
  }

  public final void setCapacity(final int capacity) {
    this.capacity = capacity;
  }

  public final void setLoadCapacity(final int loadCapacity) {
    this.loadCapacity = loadCapacity;
  }

  public final int getTravelDistance() {
    return travelDistance;
  }

  public final void setTravelDistance(final int travelDistance) {
    this.travelDistance = travelDistance;
  }

  public abstract String fly();

  public abstract String getInfo();
}