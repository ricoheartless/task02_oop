package com.gromovyi.model;

public class CargoPlane extends Aeroplane {

  public CargoPlane(final int capacity, final int loadCapacity,
      final int fuelTank,
      final int travelDistance) {
    super(capacity, loadCapacity, fuelTank, travelDistance);
  }

  public final String fly() {
    return "Cargo plane is flying now";
  }

  @Override
  public final String getInfo() {
    return "Capacity: " + getCapacity()
        + " Fuel tank: " + getFuelTank()
        + " Cargo capacity: " + getLoadCapacity()
        + " Travel distance: " + getTravelDistance();
  }
}
