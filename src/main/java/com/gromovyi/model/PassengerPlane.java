package com.gromovyi.model;

public class PassengerPlane extends Aeroplane {

  private int seats;
  private static final int SEATS_NUM = 50;

  public PassengerPlane(final int capacity, final int cargoCapacity,
      final int fuelTank, final int travelDistance) {
    super(capacity, cargoCapacity, fuelTank, travelDistance);
    this.seats = SEATS_NUM;
  }

  public final int getSeats() {
    return seats;
  }

  public final void setSeats(final int seats) {
    this.seats = seats;
  }

  public final String fly() {
    return "Passenger plane is flying";
  }

  @Override
  public final String getInfo() {
    return "Capacity: " + getCapacity()
        + " Fuel tank: " + getFuelTank()
        + " Cargo capacity: " + getLoadCapacity()
        + " Travel distance: " + getTravelDistance()
        + " Seats: " + getSeats();
  }
}
