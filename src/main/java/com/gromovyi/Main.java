package com.gromovyi;

import com.gromovyi.manager.AviaCompany;
import com.gromovyi.utility.Creator;
import com.gromovyi.utility.Range;
import java.util.List;
import java.util.Scanner;

public final class Main {

  private Main() {
  }

  private static final void showStartMessage() {
    System.out.println("Enter '1' - to show all aeroplanes");
    System.out.println("Enter '2' - to sort all aeroplanes by distance");
    System.out.println("Enter '3' - to find aeroplane by fuel consume");
    System.out.println("Enter '4' - to show sum of capacity and cargo load");
    System.out.println("Enter '5' - to show help");
    System.out.println("Enter '0' - to exit");
  }

  public static void main(final String[] args) {
    List aeroplanes = Creator.createAeroplanes();
    AviaCompany aviaCompany = new AviaCompany(aeroplanes);
    Scanner scanner = new Scanner(System.in);
    int input;
    showStartMessage();
    do {
      while (!scanner.hasNextInt()) {
        scanner.next();
      }
      input = scanner.nextInt();
      switch (input) {
        case 0:
          continue;
        case 1:
          aviaCompany.show();
          break;
        case 2:
          aviaCompany.sortByDistance();
          System.out.println("Sorted successfully!");
          break;
        case 3:
          System.out.println("Enter range to find aeroplane: ");
          int min = scanner.nextInt();
          int max = scanner.nextInt();
          AviaCompany result = new AviaCompany(aviaCompany.findByFuelConsume(new Range(min, max)));
          result.show();
          break;
        case 4:
          System.out.println("Sum of capacity: "
              + aviaCompany.getSumOfCapacity());
          System.out.println("Sum of cargo load capacity: "
              + aviaCompany.getSumOfCargoLoad());
          break;
        case 5:
          showStartMessage();
          break;
        default:
          System.out.println("Please enter correct num! Enter 5 to see menu");
          break;
      }
    }
    while (input != 0);
  }
}
